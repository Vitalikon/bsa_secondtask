﻿using System;
using System.IO;

namespace CoolParking.BL.Utilities
{
    public static class InputValidators
    {
        public static int IntegerInput()
        {
            var isSuccessParse = int.TryParse(Console.ReadLine(), out var input);
            if (!isSuccessParse)
            {
                throw new IOException("Wrong input!");
            }

            return input;
        }

        public static int IntegerInput(int minValue, int maxValue = int.MaxValue)
        {
            var input = IntegerInput();
            if (input < minValue || input > maxValue)
            {
                throw new ArgumentOutOfRangeException($"input", "Out of Range!");
            }

            return input;
        }

        public static decimal DecimalInput()
        {
            var isSuccessParse = decimal.TryParse(Console.ReadLine(), out var input);
            if (!isSuccessParse)
            {
                throw new IOException("Wrong input!");
            }

            return input;
        }
        public static decimal DecimalInput(decimal minValue, decimal maxValue = decimal.MaxValue)
        {
            var input = DecimalInput();
            if (input < minValue || input > maxValue)
            {
                throw new ArgumentOutOfRangeException($"input", "Out of Range!");
            }

            return input;
        }

        public static string StringInput()
        {
            var str = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(str))
            {
                throw new IOException("Wrong input!");
            }

            return str;
        }
        
        
    }
}