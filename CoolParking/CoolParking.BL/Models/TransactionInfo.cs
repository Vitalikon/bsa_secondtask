﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.


using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public decimal Sum { get; }
        public DateTime Time { get; }
        public string VehicleId { get; }


        public TransactionInfo(decimal sum, string vehicleId)
        {
            Sum = sum;
            VehicleId = vehicleId;
            Time = DateTime.Now;
        }

        public override string ToString()
        {
            return $"Time: {Time}; Vehicle Id: {VehicleId}; Income: {Sum}";
        }
    }
}