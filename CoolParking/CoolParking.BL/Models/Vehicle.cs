﻿// TODO: implement class Vehicle.
//+       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//+       The format of the identifier is explained in the description of the home task.
//+       Id and VehicleType should not be able for changing.
//+       The Balance should be able to change only in the CoolParking.BL project.
//+       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//+       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }


        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (string.IsNullOrWhiteSpace(id) || !IsMatchIdStringFormat(id))
                throw new ArgumentException("Vehicle.Id does not match format");
            Id = id;
            if (balance <= 0m)
            {
                throw new ArgumentException("Vehicle initial balance cannot be less than 0");
            }

            Balance = balance;
            VehicleType = vehicleType;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var strBuilder = new StringBuilder();
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random = new Random();
            for (var i = 0; i < 10; i++)
            {
                if (i is < 2 or > 7)
                    strBuilder.Append(letters[random.Next(letters.Length)]);
                else if (i is 2 or 7)
                    strBuilder.Append('-');
                else
                    strBuilder.Append(random.Next(0, 9));
            }

            return strBuilder.ToString();
        }

        public static bool IsMatchIdStringFormat(string str)
        {
            return Regex.IsMatch(str, @"^([A-Z]{2})-([0-9]{4})-([A-Z]{2})$");
        }
    }
}