﻿using System;

namespace CoolParking.ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var showMenu = true;
            while (showMenu)
            {
                Console.Clear();
                showMenu = ConsoleMenu.ShowMenu();
                Console.WriteLine("\nPress any button to continue");
                Console.ReadKey();
            }
        }
    }
}