﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using CoolParking.BL.Utilities;

namespace CoolParking.ConsoleApplication
{
    internal static class ConsoleMenu
    {
        private static readonly ParkingService ParkingService = new ParkingService(
            new TimerService(),
            new TimerService(),
            new LogService(Settings.LoggerFilePath));

        public static bool ShowMenu()
        {
            try
            {
                Console.WriteLine("1. Show parking balance\n" +
                                  "2. Show profit for the current period\n" +
                                  "3. Show parking capacity\n" +
                                  "4. Show all parking transactions for the current period\n" +
                                  "5. Show transactions history\n" +
                                  "6. Show list of parked vehicles\n" +
                                  "7. Park vehicle\n" +
                                  "8. Remove vehicle from parking\n" +
                                  "9. Top up vehicle balance\n" +
                                  "0. Exit");
                Console.Write("\nSelect an option: ");
                var choice = InputValidators.IntegerInput(0, 9);
                Console.Clear();

                switch (choice)
                {
                    case 1:
                        ShowParkingBalance();
                        return true;
                    case 2:
                        ShowCurrentProfit();
                        return true;
                    case 3:
                        ShowParkingCapacity();
                        return true;
                    case 4:
                        ShowCurrentParkingTransactions();
                        return true;
                    case 5:
                        ShowTransactionsHistoryFromLogs();
                        return true;
                    case 6:
                        ShowParkedVehicles();
                        return true;
                    case 7:
                        ParkVehicle();
                        return true;
                    case 8:
                        RemoveVehicle();
                        return true;
                    case 9:
                        TopUpVehicle();
                        return true;
                    case 0:
                        return false;
                }
            }
            catch (Exception e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }

            return true;
        }

        // 1
        private static void ShowParkingBalance()
        {
            Console.WriteLine($"Parking balance: {ParkingService.GetBalance()}");
        }

        // 2
        private static void ShowCurrentProfit()
        {
            Console.WriteLine($"Current profit: {ParkingService.GetLastParkingTransactions().Sum(t => t.Sum)}");
        }

        // 3
        private static void ShowParkingCapacity()
        {
            Console.WriteLine($"Capacity: {ParkingService.GetCapacity()}\n" +
                              $"Free places: {ParkingService.GetFreePlaces()}");
        }

        // 4
        private static void ShowCurrentParkingTransactions()
        {
            var builder = new StringBuilder();
            foreach (var t in ParkingService.GetLastParkingTransactions())
            {
                builder.Append($"{t}\n");
            }

            if (builder.Length == 0)
            {
                Console.WriteLine("No transactions.");
            }
            else
            {
                Console.WriteLine(builder.ToString());
            }
        }

        // 5
        private static void ShowTransactionsHistoryFromLogs()
        {
            var transactions = ParkingService.ReadFromLog();
            if (string.IsNullOrWhiteSpace(transactions))
            {
                Console.WriteLine("No transactions.");
            }
            else
            {
                Console.WriteLine(transactions);
            }
        }

        public static void DisplayVehicleTable(ReadOnlyCollection<Vehicle> vehicles)
        {
            if (vehicles.Count == 0)
            {
                Console.WriteLine("No parked vehicles.");
                return;
            }

            var counter = 1;
            Console.WriteLine($"|{"#",-3}|{"ID",-10}|{"Balance",-7}|{"Vehicle Type",-12}|");
            Console.WriteLine("_____________________________________");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"|{counter++,-3}|{vehicle.Id,-10}|{vehicle.Balance,-7}|{vehicle.VehicleType,-12}|");
            }
        }

        // 6
        private static void ShowParkedVehicles()
        {
            var vehicles = ParkingService.GetVehicles();
            DisplayVehicleTable(vehicles);
        }

        // 7
        private static void ParkVehicle()
        {
            Console.Write("Enter new vehicle Id (AA-0001-AA): ");
            var vehicleId = InputValidators.StringInput().ToUpper();
            if (!Vehicle.IsMatchIdStringFormat(vehicleId))
            {
                throw new ArgumentException("Vehicle Id does not match format");
            }

            Console.Write("Choose type of vehicle:\n" +
                          "1 - PassengerCar\n" +
                          "2 - Truck\n" +
                          "3 - Bus\n" +
                          "4 - Motorcycle\n>");
            var vehicleType = (VehicleType) InputValidators.IntegerInput(1, 4) - 1;

            Console.Write("Write initial vehicle balance: ");
            var vehicleBalance = InputValidators.DecimalInput(0);
            ParkingService.AddVehicle(new Vehicle(vehicleId, vehicleType, vehicleBalance));
            Console.WriteLine("Vehicle parked.");
        }

        // 8
        private static void RemoveVehicle()
        {
            var vehicles = ParkingService.GetVehicles();
            DisplayVehicleTable(vehicles);
            Console.Write("\nSelect vehicle to remove by #-number: ");
            var vehicleIndex = InputValidators.IntegerInput(1, vehicles.Count);
            ParkingService.RemoveVehicle(ParkingService.GetVehicles()[vehicleIndex - 1].Id);
            Console.WriteLine("Vehicle removed.");
        }

        //9
        public static void TopUpVehicle()
        {
            var vehicles = ParkingService.GetVehicles();
            DisplayVehicleTable(vehicles);
            Console.Write("\nSelect vehicle for top up by #-number: ");
            var vehicleIndex = InputValidators.IntegerInput(1, vehicles.Count);
            Console.Write("Enter top up amount: ");
            var sum = InputValidators.DecimalInput(0m);
            ParkingService.TopUpVehicle(ParkingService.GetVehicles()[vehicleIndex - 1].Id, sum);
            Console.WriteLine("Vehicle topped up.");
        }
    }
}